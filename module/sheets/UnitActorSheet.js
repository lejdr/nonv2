export default class UnitActorSheet extends ActorSheet 
{
    get template() {
        console.log(`systems/YadaFoundryVTT/templates/sheets/${this.item.data.type}-sheet.html`);
        return (`systems/YadaFoundryVTT/templates/sheets/unit-sheet.html`);
    }

    getData() {
        const baseData = super.getData();
        let sheetData = {
            config: CONFIG.yada
        }

        return sheetData;
    }
}