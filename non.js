import { non } from "./module/config.js";
import unitActorSheet from "./module/sheets/UnitActorSheet.js";

Hooks.once("init", function() 
{
    console.log("non | Initialising non System");

    CONFIG.non = non;

    Actors.unregisterSheet("core", ActorSheet);
    Actors.registerSheet("unit", UnitActorSheet, {makeDefault: true});
});